HEADERS += \
    TScene.h \
    TMainwindow.h \
    TGame.h \
    demoscene.h

SOURCES += \
    TScene.cpp \
    TMainwindow.cpp \
    TGame.cpp \
    main.cpp

FORMS += \
    mainwindow.ui
