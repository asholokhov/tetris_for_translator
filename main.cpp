#include <iostream>
#include <cstdlib>

#include <TGame.h>
#include <TMainwindow.h>

using namespace std;

QApplication *_app;
TMainWindow  *_wnd;

int main(int argc, char* argv[]) {
    // Создание Qt-приложения
    _app = new QApplication(argc, argv);

        // Создаем и показываем главное окно программы
        _wnd = new TMainWindow();
        _wnd->show();

    return (_app->exec());
}
