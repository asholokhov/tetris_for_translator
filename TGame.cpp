#include "TGame.h"
#include "TMainwindow.h"

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>

#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QString>

#include <queue>

using namespace std;

extern QApplication *_app;
extern TMainWindow  *_wnd;

// Translator vars
extern bool glb_is_demo_set;
extern queue <figure_queue_item> figures_list;
extern queue <TDirection> moves_list;
//

// Игровой конструктор - начальная инициализация полей классов
TGame::TGame(string username, TScene *a_canvas, TNextFigureScene *a_next_figure_canvas) :
    m_canvas(a_canvas),
    m_next_canvas(a_next_figure_canvas),
    m_player_name(username)
{
     init_game();
}

// Инициализация начальных условий и массивов
void TGame::init_game() {
    m_level = 1;
    m_score = 0;
    m_sleep_time = DEFAULT_SLEEP_TIME;

    for (int i = 0; i < SCENE_HEIGHT; i++)
        for (int j = 0; j < SCENE_WIDTH; j++)
            m_game_map[i][j] = 0;

    m_terminated = false;

    m_canvas->init_scene();
    m_current_shape.clear_shape();

    m_timer = new QTimer(this);
    m_timer->start(m_sleep_time);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(process_game()));

    _wnd->updateScores(m_score, m_level);
}

// Функция проверяет, проиграл ли игрок в данный момент времени
bool TGame::is_defeated() const {
    bool defeated = false;

    for (int i = 0; i < 2; i++)
        for (int j = 0; j < SCENE_WIDTH; j++)
            if (defeated) break; else
            if (m_game_map[i][j] != 0) {
                defeated = true;
                break;
            }

    return defeated;
}

// Запуск отрисовки и игрового процесса
void TGame::start() {
    qDebug() << "Start drawing and processing game.";
    m_canvas->start_draw();

    while (!m_terminated) {
        while (_app->hasPendingEvents())
            _app->processEvents();
    }

    m_timer->stop();
    m_canvas->stop_draw();
    qDebug() << "End game.";
}

// Итерация игрового процесса
void TGame::process_game() {
    qDebug() << "Process game. Now sleep time is: " << m_sleep_time << "ms.";
    if (!m_terminated) { // если игра не прервана (игрок не проиграл или не нажал выход)

        bool demo_end = false;
        if (m_current_shape.need_new_unit()) { // проверяем, требуется ли новая фигурка
            // если требуется новая фигурка, то переносим предыдущую фигурку
            // на игровую карту
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                    if (m_current_shape.shape_map[i][j] != 0)
                        m_game_map[m_current_shape.get_y() + i][m_current_shape.get_x() + j] =
                                m_current_shape.shape_map[i][j];

            // создаем новую фигурку и устанавливаем ее как текущую
            if (glb_is_demo_set) {
                if (!figures_list.empty()) {
                    int x = figures_list.front().x;
                    int y = figures_list.front().y;
                    TUnitType t = figures_list.front().type;

                    m_current_shape.m_x = x;
                    m_current_shape.m_y = y;
                    m_current_shape.m_rotate_flag        = 0;
                    m_current_shape.m_need_new_unit      = false;
                    m_current_shape.m_current_shape_type = t;
                    m_current_shape.draw_shape_on_shape_map();

                    figures_list.pop();
                } else {
                    QMessageBox::information(0, tr("Demoscene"), tr("Demoscene is over!"), QMessageBox::Ok);
                    demo_end = true;
                }

            } else {
                m_current_shape.spawn_unit(int(SCENE_WIDTH/2) - 1, -1);
                m_next_canvas->set_figure(m_current_shape.get_next_figure_type());
            }
        }

        // добавляем очки игрку, если ряд заполнен
        int add_scores = remove_filled_lines();
        m_score += add_scores;
        if (add_scores > 0)
            if ((m_score >= 1000) && (m_score % 1000 == 0))
                next_level();

        // обновляем отрисовку очков игрока
        _wnd->updateScores(m_score, m_level);
        _wnd->update();

        // устанваливаем карту для отрисовки в объект-сцену
        m_canvas->set_objects(m_game_map,
                              m_current_shape.shape_map,
                              m_current_shape.get_x(),
                              m_current_shape.get_y());

        if (glb_is_demo_set) {
            if (!moves_list.empty()) {
                TDirection dir = moves_list.front();
                moves_list.pop();

                if (dir != D_STAY)
                    move_shape(dir);
            }
        }

        // делаем итерацию фигурки - опускаем ее вниз на 1 клетку
        move_shape(D_DOWN);
        // проверяем, проиграл ли игрок на текущей итерации
        m_terminated = is_defeated();

        // если игра была прервана (условие - игрок проиграл)
        if (m_terminated || demo_end) {
            m_terminated = true;
            // выводим уведомление о проигрыше и показываем очки пользователя
            QString str(QString(tr("You lose! Scores: %1")).arg(QString::number(m_score)));
            QMessageBox::information(0, tr("Game Over"), str, QMessageBox::Ok);
            // сохраняем в таблицу рекордов
            save_scores();
        }
    }
}

// увеличение уровня и сложности игры
// уменьшается время одной итерации
// если время стало нулевым, тогда считается,
// что игрок выиграл - останавливаем таймеры
// итерации игры и отрисовки
void TGame::next_level() {
    m_level++;
    m_sleep_time -= SLEEP_TIME_PER_LVL;
    m_timer->stop();
    if (m_sleep_time <= 0) {
        QMessageBox::information(0, tr("Congritulations!"),
                                 tr("You win! Scores: "),
                                 QMessageBox::Ok);
        m_terminated = true;
        m_timer->stop();
    } else {
        m_timer->start(m_sleep_time);
    }
    qDebug() << "Level up to " << m_level;
}

// функция, удаляющая заполненные игроком ряды
// и возвращающая количество очков, набранных
// за каждый ряд суммарно
int TGame::remove_filled_lines() {
    int removed_lines = 0;
    for (int i = 0; i < SCENE_HEIGHT; i++) {
        bool f_need_to_remove = true;
        for (int j = 0; j < SCENE_WIDTH; j++) {
            if (m_game_map[i][j] != 1) {
                f_need_to_remove = false;
                break;
            }
        }
        if (f_need_to_remove) {
            for (int j = 0; j < SCENE_WIDTH; j++)
                m_game_map[i][j] = 0;
            shift_map_after_rm(i);
            removed_lines++;
        }
    }

    return (removed_lines * SCORES_PER_LINE);
}

// сдвиг на одну клетку после удаления ряда
void TGame::shift_map_after_rm(int from_str) {
    for (int i = from_str; i >= 0; i--) {
        for (int j = 0; j < SCENE_WIDTH; j++) {
            if (i - 1 >= 0)
                m_game_map[i][j] = m_game_map[i - 1][j];
            else
                m_game_map[i][j] = 0;
        }
    }
}

// функция-сравнение для сортировки результатов
bool compare(std::pair<string, int> i, std::pair<string, int> j) {
    return (i.second > j.second);
}

// сохраняет результат игока в таблицу результатов
// в отсортированном по убыванию виде
void TGame::save_scores() {
    qDebug() << "Save result.";
    vector < std::pair<string, int> > results;
    std::ifstream in("scores.txt");
    int rec_count = 0;
    in >> rec_count;
    for (int i = 0; i < rec_count; i++) {
        string name;
        int score;
        in >> name >> score;
        results.push_back(std::make_pair(name, score));
    }

    results.push_back(std::make_pair(m_player_name, m_score));
    std::sort(results.begin(), results.end(), compare);
    in.close();

    std::ofstream out("scores.txt");
    out << rec_count + 1 << endl;
    for (int i = 0; i < results.size(); i++)
        out << results[i].first << " " << results[i].second << endl;
    out.close();
}

// TShape

// создает новую фигурку на карте с координатами X,Y
void TShape::spawn_unit(int x, int y) {
    m_x = x;
    m_y = y;
    m_rotate_flag        = 0;
    m_need_new_unit      = false;
    m_current_shape_type = static_cast<TUnitType>(m_next_figure);
    m_next_figure        = rand()%7;
    draw_shape_on_shape_map();

    qDebug() << "Spawned unit type: " << static_cast<int>(m_current_shape_type);
    qDebug() << "Next unit type: " << static_cast<int>(m_next_figure);
    qDebug() << "Unit real size: " << m_real_w << "x" << m_real_h;
}

// отрисовывает фигурку на карту для фигурки в зависимости
// от текущего типа фигурки
void TShape::draw_shape_on_shape_map() {
    clear_shape();
    switch (m_current_shape_type) {
        case U_UNDEFINDED:
        case U_SPAWNERROR:
            exit(GAME_INTERNAL_ERR);
        break;

        case U_SQUARE: {
            // one state
            shape_map[0][0] = 1; // 1100
            shape_map[0][1] = 1; // 1100
            shape_map[1][0] = 1; // 0000
            shape_map[1][1] = 1; // 0000
            m_real_w        = 2;
            m_real_h        = 2;
        }
        break;

        case U_SQUIGGLE:  {
                switch (m_rotate_flag) {
                    case 0:
                    default:
                        shape_map[0][1] = 1; // 0100
                        shape_map[1][0] = 1; // 1100
                        shape_map[1][1] = 1; // 1000
                        shape_map[2][0] = 1; // 0000
                        m_real_w        = 2;
                        m_real_h        = 3;
                        break;
                    case 1:
                        shape_map[0][0] = 1; // 1100
                        shape_map[0][1] = 1; // 0110
                        shape_map[1][1] = 1; // 0000
                        shape_map[1][2] = 1; // 0000
                        m_real_w        = 3;
                        m_real_h        = 2;
                        break;
                }
        }
        break;

        case U_RSQUIGGLE: {
                switch (m_rotate_flag) {
                    case 0:
                    default:
                        shape_map[0][0] = 1; // 1000
                        shape_map[1][0] = 1; // 1100
                        shape_map[1][1] = 1; // 0100
                        shape_map[2][1] = 1; // 0000
                        m_real_w        = 2;
                        m_real_h        = 3;
                        break;
                    case 1:
                        shape_map[0][1] = 1; // 0110
                        shape_map[0][2] = 1; // 1100
                        shape_map[1][0] = 1; // 0000
                        shape_map[1][1] = 1; // 0000
                        m_real_w        = 3;
                        m_real_h        = 2;
                        break;
                }
        }
        break;

        case U_LBLOCK: {
                switch (m_rotate_flag) {
                    case 0:
                    default:
                        shape_map[0][0] = 1; // 1000
                        shape_map[1][0] = 1; // 1000
                        shape_map[2][0] = 1; // 1100
                        shape_map[2][1] = 1; // 0000
                        m_real_w        = 2;
                        m_real_h        = 3;
                        break;
                    case 1:
                        shape_map[0][2] = 1; // 0010
                        shape_map[1][0] = 1; // 1110
                        shape_map[1][1] = 1; // 0000
                        shape_map[1][2] = 1; // 0000
                        m_real_w        = 3;
                        m_real_h        = 2;
                        break;
                    case 2:
                        shape_map[0][0] = 1; // 1100
                        shape_map[0][1] = 1; // 0100
                        shape_map[1][1] = 1; // 0100
                        shape_map[2][1] = 1; // 0000
                        m_real_w        = 2;
                        m_real_h        = 3;
                        break;
                    case 3:
                        shape_map[0][0] = 1; // 1110
                        shape_map[0][1] = 1; // 1000
                        shape_map[0][2] = 1; // 0000
                        shape_map[1][0] = 1; // 0000
                        m_real_w        = 3;
                        m_real_h        = 2;
                        break;
                }
        }
        break;

        case U_RLBLOCK: {
                switch (m_rotate_flag) {
                    case 0:
                    default:
                        shape_map[0][0] = 1; // 1100
                        shape_map[0][1] = 1; // 1000
                        shape_map[1][0] = 1; // 1000
                        shape_map[2][0] = 1; // 0000
                        m_real_w        = 2;
                        m_real_h        = 3;
                        break;
                    case 1:
                        shape_map[0][0] = 1; // 1000
                        shape_map[1][0] = 1; // 1110
                        shape_map[1][1] = 1; // 0000
                        shape_map[1][2] = 1; // 0000
                        m_real_w        = 3;
                        m_real_h        = 2;
                        break;
                    case 2:
                        shape_map[0][1] = 1; // 0100
                        shape_map[1][1] = 1; // 0100
                        shape_map[2][0] = 1; // 1100
                        shape_map[2][1] = 1; // 0000
                        m_real_w        = 2;
                        m_real_h        = 3;
                        break;
                    case 3:
                        shape_map[0][0] = 1; // 1110
                        shape_map[0][1] = 1; // 0010
                        shape_map[0][2] = 1; // 0000
                        shape_map[1][2] = 1; // 0000
                        m_real_w        = 3;
                        m_real_h        = 2;
                        break;
                }
        }
        break;

        case U_TBLOCK: {
                switch (m_rotate_flag) {
                    case 0:
                    default:
                        shape_map[0][1] = 1; // 0100
                        shape_map[1][0] = 1; // 1110
                        shape_map[1][1] = 1; // 0000
                        shape_map[1][2] = 1; // 0000
                        m_real_w        = 3;
                        m_real_h        = 2;
                        break;
                    case 1:
                        shape_map[0][1] = 1; // 0100
                        shape_map[1][0] = 1; // 1100
                        shape_map[1][1] = 1; // 0100
                        shape_map[2][1] = 1; // 0000
                        m_real_w        = 2;
                        m_real_h        = 3;
                        break;
                    case 2:
                        shape_map[0][0] = 1; // 1110
                        shape_map[0][1] = 1; // 0100
                        shape_map[0][2] = 1; // 0000
                        shape_map[1][1] = 1; // 0000
                        m_real_w        = 3;
                        m_real_h        = 2;
                        break;
                    case 3:
                        shape_map[0][0] = 1; // 1000
                        shape_map[1][0] = 1; // 1100
                        shape_map[1][1] = 1; // 1000
                        shape_map[2][0] = 1; // 0000
                        m_real_w        = 2;
                        m_real_h        = 3;
                        break;
                }
        }
        break;

        case U_LINEPIECE: {
                switch (m_rotate_flag) {
                    case 0:
                    default:
                        shape_map[0][0] = 1; // 1000
                        shape_map[1][0] = 1; // 1000
                        shape_map[2][0] = 1; // 1000
                        shape_map[3][0] = 1; // 1000
                        m_real_w        = 1;
                        m_real_h        = 4;
                        break;
                    case 1:
                        shape_map[0][0] = 1; // 1111
                        shape_map[0][1] = 1; // 0000
                        shape_map[0][2] = 1; // 0000
                        shape_map[0][3] = 1; // 0000
                        m_real_w        = 4;
                        m_real_h        = 1;
                        break;
                }
        }
        break;
    }
}

// функция, ответсвенная за поворот фигурки
// используется флаг поворота - в зависимости
// от него, на карту будет отрисована необходимая
// фигурка
void TShape::rotateShape() {
    switch (m_current_shape_type) {
        case U_SQUARE:
            break; // нет необходимости вращать квадрат

        case U_SQUIGGLE :
        case U_RSQUIGGLE:
        case U_LINEPIECE:
            m_rotate_flag++;
            if (m_rotate_flag > 1)
                m_rotate_flag = 0;
            break;

        case U_LBLOCK:
        case U_RLBLOCK:
        case U_TBLOCK:
            m_rotate_flag++;
            if (m_rotate_flag > 3)
                m_rotate_flag = 0;
            break;

        default: break;
    }
    draw_shape_on_shape_map();
}


// функция проверки на столкновения
bool TShape::is_collision(TDirection dir) {
    int temp_x  = m_x; // временная координата Х
    int temp_y  = m_y; // временная координата Y
    bool need_r = false; // флаг при проверке на вращение
    bool result = false; // возвращаемый результат

    switch(dir) {
        case D_LEFT  : temp_x--; break;
        case D_RIGHT : temp_x++; break;
        case D_DOWN  : temp_y++; break;
        case D_ROTATE: // если производим вращение фигурки
            need_r = true; // то запоминаем это
            switch (m_current_shape_type) { // выбираем текущий тип фигурки
                case U_SQUIGGLE :           // изменяем значение флага-вращения
                case U_RSQUIGGLE:
                case U_LINEPIECE:
                    m_rotate_flag++;
                    if (m_rotate_flag > 1)
                        m_rotate_flag = 0;
                    break;

                case U_LBLOCK:
                case U_RLBLOCK:
                case U_TBLOCK:
                    m_rotate_flag++;
                    if (m_rotate_flag > 3)
                        m_rotate_flag = 0;
                    break;

                default: break;
            }
            draw_shape_on_shape_map(); // отрисовываем повернутую фигурку на карту для фигур

            if ((m_x + (m_real_w - 1) + 1 > SCENE_WIDTH)) // если повернутая фигура не влазит
                result = true;                            // в экран, сообщаем об этом

            break;
    }

    for (int i = 0; i < 4; i++) { // проверка на столкновения между фигурой и
        for (int j = 0; j < 4; j++) {   // статическими объектами на карте
            if (result) break;
            if (shape_map[i][j] && m_game_map[temp_y + i][temp_x + j]) {
                qDebug() << "Collision find! Direction: " << static_cast<int>(dir);
                result = true;
                break;
            }
        }
    }

    if (need_r) { // если проверялось вращение, то возвращаем флаг поворота на свое место
        switch (m_current_shape_type) {
            case U_SQUIGGLE :
            case U_RSQUIGGLE:
            case U_LINEPIECE:
                m_rotate_flag--;
                if (m_rotate_flag < 0)
                    m_rotate_flag = 1;
                break;

            case U_LBLOCK:
            case U_RLBLOCK:
            case U_TBLOCK:
                m_rotate_flag--;
                if (m_rotate_flag < 0)
                    m_rotate_flag = 3;
                break;

            default: break;
        }
        draw_shape_on_shape_map(); // заново перерисовываем фигуру
    }

    return result; // возвращаем результат проверки коллизии
}

// функция, которая двигает фигурку в зависимости от напрвления dir
// Влево, Вправо, Вверх, Вращение
void TShape::move(TDirection dir) {
    switch (dir) {
        case D_LEFT:
            if ((m_x - 1 >= 0)&&(!is_collision(D_LEFT)))
                m_x--;
            break;

        case D_RIGHT:
            if ((m_x + (m_real_w - 1) + 1 < SCENE_WIDTH)&&(!is_collision(D_RIGHT)))
                m_x++;
            break;

        case D_DOWN:
            if (((m_y + (m_real_h - 1) + 1 < SCENE_HEIGHT))&&(!is_collision(D_DOWN))) {
                m_y++;
            } else {
                // figure must be placed on game map and spawn new unit
                m_need_new_unit = true;
            }
            break;

        case D_ROTATE:
            if (!is_collision(D_ROTATE))
                rotateShape();
            break;
    }
}
