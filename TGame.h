#ifndef TGAME_H
#define TGAME_H

#include <TScene.h>

#include <iostream>
#include <string>

using namespace std;

#define SCORES_PER_LINE    100
#define DEFAULT_SLEEP_TIME 1000
#define SLEEP_TIME_PER_LVL 50

#define GAME_NORMAL_STATE  1004
#define GAME_DEFEAT_FLAG   1005
#define GAME_INTERNAL_ERR  1006

// тип фигуры для идентификации - квадрат, зигзаг...
enum TUnitType {
    U_UNDEFINDED=-2,
    U_SPAWNERROR=-1,
    U_SQUARE    = 0,
    U_SQUIGGLE  = 1,
    U_RSQUIGGLE = 2,
    U_LBLOCK    = 3,
    U_RLBLOCK   = 4,
    U_TBLOCK    = 5,
    U_LINEPIECE = 6
};

struct figure_queue_item {
    TUnitType type;
    int x;
    int y;

    figure_queue_item(int l_x, int l_y, TUnitType t) :
        x(l_x), y(l_y), type(t) {}
};

// направление движения текущей фигуры
// влево, вправо, вниз, вращение
enum TDirection {
    D_LEFT  = 0,
    D_RIGHT = 1,
    D_DOWN  = 2,
    D_ROTATE= 3,
    D_STAY  = 4 // used for skip
};

class TShape {
public:
    int m_x;    // координата Х текущей фигуры
    int m_y;    // координата Y текущей фигуры
    int m_rotate_flag;  // флаг поворота фигуры (влияет на отрисовку)
    bool m_need_new_unit; // флаг - требуется ли создание новой фигуры?

    int m_real_w; // реальная длина рисуемой фигуры (карта 4х4)
    int m_real_h; // реальная высота

    TUnitType m_current_shape_type; // тип текущей фигуры - квардат, зигзаг...
    int  m_next_figure; // тип следующей фигуры

    int m_game_map[SCENE_HEIGHT][SCENE_WIDTH]; // игровая карта, для отрисовки (и только)

    void draw_shape_on_shape_map(); // отрисовка фигуры в массив

    TShape() : m_x(0), m_y(0),
        m_rotate_flag(0), m_need_new_unit(true),
        m_current_shape_type(U_UNDEFINDED),
        m_next_figure(rand()%7) {}

    // карта текущей фигуры
    int shape_map[4][4]; // TODO: move to private sections and write return method

    // очистка карты текущей фигуры
    void clear_shape() { // TODO: move outside
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                shape_map[i][j] = 0;
    }

    // методы получения координат X и Y
    int get_x() { return m_x; }
    int get_y() { return m_y; }

    // получить реальную ширину и высоту фигуры
    int get_real_w() { return m_real_w; }
    int get_real_h() { return m_real_h; }

    // повернуть фигуры
    void rotateShape();
    // создать новую фигуру
    void spawn_unit(int x, int y);
    // проверка - требуется ли создать новую фигуру?
    bool need_new_unit() const { return m_need_new_unit; }
    // получить следующий тип фигуры
    int  get_next_figure_type() const { return m_next_figure; }
    // движение фигурки в направлении dir
    void move(TDirection dir);
    // установка игровой карты для отрисовки
    void set_game_map(int a_map[SCENE_HEIGHT][SCENE_WIDTH]) { // TODO: move outside
        for (int i = 0; i < SCENE_HEIGHT; i++)
            for (int j = 0; j < SCENE_WIDTH; j++)
                m_game_map[i][j] = a_map[i][j];
    }
    // проверка фигуры на столкновение с другими объектами карты
    bool is_collision(TDirection dir);
};

class TGame : public QObject {
    Q_OBJECT

public:
    TScene *m_canvas;   // сцена для отрисовки процесса игры
    TNextFigureScene *m_next_canvas; // сцена для отрисовки следующей фигуры

    string  m_player_name; // имя игрока
    int     m_level; // текущей уровень игры
    int     m_score; // игровые очки
    int     m_sleep_time; // время обновления (итерации)
    int     m_game_map[SCENE_HEIGHT][SCENE_WIDTH]; // игровая карта (стандарт - 20х10)
    bool    m_terminated; // флаг - прервана игра или нет

    TShape m_current_shape; // текущая фигура с которой работаем

    QTimer *m_timer; // таймер - отдельный поток для итерации игры

    // перевести игру на следующий уровень (ускорить темп)
    void next_level();
    // удалить заполненные игроком ряды
    int remove_filled_lines();
    // проверка на проигрыш
    bool is_defeated() const;
    // свиг всех рядов начиная с from_str
    // после удаления на 1 клетку вниз
    void shift_map_after_rm(int from_str);
    // инициализация игры
    void init_game();
    // сохранение игровых очков
    void save_scores();

private slots:
    // итерация игры
    void process_game();

public:
    TGame(string username, TScene *a_canvas, TNextFigureScene *a_next_figure_canvas);

    // начать игру
    void  start();
    // прервать игру
    void terminate() { m_terminated = true; }
    // получить количество очков
    int  get_scores() const { return m_score; }
    // передвинуть фигурку по направлению dir
    void move_shape(TDirection dir) {
        m_current_shape.set_game_map(m_game_map);
        m_current_shape.move(dir);
        m_canvas->set_objects(m_game_map,
                              m_current_shape.shape_map,
                              m_current_shape.get_x(),
                              m_current_shape.get_y());
    }
    // тестовый вывод
    void debug_out() const {
        qDebug() << "Game map now look like:";
        for (int i = 0; i < SCENE_HEIGHT; i++) {
            for (int j = 0; j < SCENE_WIDTH; j++)
                cout << m_game_map[i][j];
            cout << endl;
        }
        cout << endl;
    }
};

#endif // TGAME_H
