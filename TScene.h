#ifndef TSCENE_H
#define TSCENE_H

#include <QFrame>
#include <QPainter>
#include <QDebug>
#include <QTimer>

#include <iostream>

using std::cout;
using std::endl;

// Default settings
#define SCENE_UPDATE_TIME 50

#define SCENE_WIDTH  10
#define SCENE_HEIGHT 20

class TScene : public QFrame {
    Q_OBJECT

    bool   m_started; // флаг запуска отрисовки
    int    m_block_w; // ширина рисуемого блока
    int    m_block_h; // высота рисуемого блока
    int    m_fps;     // кадров в секунду

    // рисуемуя сцена
    int    m_drawing_scene[SCENE_HEIGHT][SCENE_WIDTH];
    // рисуемая фигура
    int    m_drawing_shape[4][4];

    int    m_shape_x; // координата X фигуры
    int    m_shape_y; // координата Y фигуры

    QTimer *m_timer;  // поток отрисовки
public:
    TScene(QWidget *parent = 0) : QFrame(parent),
        m_started(false) {}
    ~TScene() { if (m_timer) delete m_timer; }

    // инициализация сцены
    void init_scene();
    // старт отрисовки
    void start_draw()        { m_started = true;  }
    // закончить отрисовку
    void stop_draw()         { m_started = false; }
    // установить рисуемые объекты на карту
    void set_objects(int a_drawing_scene[SCENE_HEIGHT][SCENE_WIDTH],
                     int a_drawing_shape[4][4],
                     int x,
                     int y) {
        for (int i = 0; i < SCENE_HEIGHT; i++)
            for (int j = 0; j < SCENE_WIDTH; j++)
                m_drawing_scene[i][j] = a_drawing_scene[i][j];

        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                m_drawing_shape[i][j] = a_drawing_shape[i][j];
        
        m_shape_x = x;
        m_shape_y = y;
    }
    // установить частоту обновления отрисовки
    void set_fps(int fps) {
        m_fps = fps;
        m_timer->setInterval(fps);
    }

protected:
    // событие отрисовки
    void paintEvent(QPaintEvent *event);
};

class TNextFigureScene : public QFrame {
    Q_OBJECT

    bool  m_started; // флаг отрисовки
    int   m_figure_type; // тип рисуемой фигуры
    int   m_shape_map[4][4]; // рисуемая карта
    QTimer *m_timer; // поток отрисовки
public:
    TNextFigureScene(QWidget *parent = 0) : QFrame(parent) {
        m_started = true;
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                m_shape_map[i][j] = 0;
        m_timer = new QTimer(this);
        connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));
        m_timer->start(SCENE_UPDATE_TIME);
    }

    void stop_drawing()           { m_started = false;        }
    void set_figure(int new_type);

protected:
    void paintEvent(QPaintEvent *);
};

#endif // TSCENE_H
