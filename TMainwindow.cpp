#include "TMainwindow.h"
#include "TScene.h"

#include <qdebug.h>
#include <fstream>
#include <string>
#include <QInputDialog>
#include <QBoxLayout>
#include <QMessageBox>

#include "demoscene.h"

extern int glb_demo_skip_count;

// конструктор главного окна
TMainWindow::TMainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow) {
    ui->setupUi(this); // устанавливаем графический интерфейс

    if (ui->tetrisOut != NULL)
        delete ui->tetrisOut; // удаляем стандартный компонент QFrame

    ui->tetrisOut = new TScene(ui->centralwidget); // вместо него создаем свой объект (наследник)
    ui->tetrisOut->setObjectName(QString::fromUtf8("tetrisOut")); // на котором будем рисовать
    ui->tetrisOut->setMinimumSize(QSize(182, 282)); // устанавливаем необходимые свойсва
//    ui->tetrisOut->setFixedSize(182, 282); // -2 for drawing area
    ui->tetrisOut->setFrameShape(QFrame::StyledPanel);
    ui->tetrisOut->setFrameShadow(QFrame::Raised);
    ui->horizontalLayout->setDirection(QBoxLayout::RightToLeft);
    ui->horizontalLayout->addWidget(ui->tetrisOut);

    // точно так же пересоздаем окно отрисовки следующей фигуры
    if (ui->nextFigureOut != NULL)
        delete ui->nextFigureOut;

    ui->nextFigureOut = new TNextFigureScene(ui->centralwidget);
    ui->nextFigureOut->setObjectName(QString::fromUtf8("nextFigureOut"));
    ui->nextFigureOut->setMinimumSize(QSize(75, 50));
    ui->nextFigureOut->setFrameShape(QFrame::StyledPanel);
    ui->nextFigureOut->setFrameShadow(QFrame::Raised);
    ui->verticalLayout->setDirection(QBoxLayout::BottomToTop);
    ui->verticalLayout->addWidget(ui->nextFigureOut);

    QLabel *nextLab = new QLabel(tr("Next figure:"));
    ui->verticalLayout->addWidget(nextLab);

    delete ui->label;

    // считаем, что игра не началась
    game = NULL;
}


// событие, отлавливающее нажатия клавиш пользователя
void TMainWindow::keyPressEvent(QKeyEvent *event) {
    if (game == NULL || glb_is_demo_set)
        return;

    switch (event->key()) {
        case Qt::Key_Left:
            game->move_shape(D_LEFT);
            break;

        case Qt::Key_Right:
            game->move_shape(D_RIGHT);
            break;

        case Qt::Key_Down:
            game->move_shape(D_DOWN);
            break;

        case Qt::Key_Up:
            game->move_shape(D_ROTATE);
            break;
    }
}

// обновление отрисовки игровых очков
void TMainWindow::updateScores(int scores, int level) {
    ui->lcdNumber->display(scores);
    ui->levelLabel->setText(tr("Level: %1").arg(QString::number(level)));
}

// обработка нажатия кнопок главного окна
void TMainWindow::on_actionNew_triggered() {
    qDebug() << "Creating new game";
    bool ok = false;
    QString usr_name = QInputDialog::getText(this, tr("New game"), tr("Input your name:"),
                                             QLineEdit::Normal, "Player", &ok);
    if (ok && !usr_name.isEmpty()) {
        qDebug() << "Name: " << usr_name;
        game = new TGame(usr_name.toUtf8().constData(),
                         reinterpret_cast<TScene*>(ui->tetrisOut),
                         reinterpret_cast<TNextFigureScene*>(ui->nextFigureOut)
                         );
        game->start();
    }
}

void TMainWindow::on_actionHigh_scores_triggered() {
    qDebug() << "Loading hight scores list";
    std::ifstream in("scores.txt");
    int count;
    in >> count;
    QString msg = "";
    for (int i = 0; i < count && i < 10; i++) {
       std::string name;
       int score;
       in >> name >> score;

       msg += QString::number(i + 1) + ". " + QString::fromStdString(name) + " " + QString::number(score) + "\n";
    }
    QMessageBox::information(this, tr("Hight scores"), msg, QMessageBox::Ok);
}

// при выходе отсанавливаем игру, очищаем ресурсы и выходим
void TMainWindow::on_actionExit_triggered() {
    game->terminate();
    close();
}

void TMainWindow::on_actionIndex_triggered() {
    qDebug() << "click";
}

void TMainWindow::on_actionAbout_triggered() {
    QMessageBox::information(this, tr("About"), tr("Sholokhov Artem, SP-01. 2013"), QMessageBox::Ok);
}

void TMainWindow::on_actionStart_demoscene_triggered() {
    start_demo();
}
