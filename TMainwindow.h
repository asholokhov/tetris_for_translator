#ifndef TMAINWINDOW_H
#define TMAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <TGame.h>

#include "ui_mainwindow.h"

namespace Ui {
    class TMainWindow;
}

class TMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    TMainWindow(QWidget *parent = 0);
    TGame *game;
    Ui::MainWindow *ui;

public slots:
    void updateScores(int scores, int level);

private slots:
    void keyPressEvent(QKeyEvent *event);
    void on_actionNew_triggered();
    void on_actionHigh_scores_triggered();
    void on_actionExit_triggered();
    void on_actionIndex_triggered();
    void on_actionAbout_triggered();
    void on_actionStart_demoscene_triggered();
};

#endif // TMAINWINDOW_H
