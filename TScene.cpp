#include "TScene.h"
#include "TGame.h"

// инициализация сцены для отрисовки объектов
void TScene::init_scene() {
    m_timer = new QTimer(this);
    m_fps   = SCENE_UPDATE_TIME;

    qDebug() << "Scene initialized. Update time: " << m_fps << "ms.";
    qDebug() << "Resolution: " << width() - 2 << "x" << height() - 2;
    qDebug() << "Start process events.";

    update();
    connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));

    m_block_w = int((width() - 1)/SCENE_WIDTH);
    m_block_h = int((height() - 1)/SCENE_HEIGHT);

    for (int i = 0; i < SCENE_HEIGHT; i++)
        for (int j = 0; j < SCENE_WIDTH; j++)
            m_drawing_scene[i][j] = 0;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            m_drawing_shape[i][j] = 0;

    m_timer->start(m_fps);
}

// переопределенное событие отрисовки, при котором рисуется
// игровая сетка и фигуры на ней
void TScene::paintEvent(QPaintEvent *event) {
    if (m_started) {
//        qDebug() << QString("Drawing... [%1 ms]").arg(QString::number(m_fps));
        QPainter qp(this);
        QColor green, pink, drcolor;
        drcolor.setRgb(136, 136, 189);
        green.setRgb(0,255,0);
        pink.setRgb(255,126,126);

        // border
        qp.drawRect(QRect(0, 0, width() - 1, height() - 1));

        // draw scene
//        cout << endl << "Drawing map:" << endl;
        for (int i = 0; i < SCENE_HEIGHT; i++) {
            for (int j = 0; j < SCENE_WIDTH; j++) {
                if (m_drawing_scene[i][j])
//                    cout << m_drawing_scene[i][j];

                    qp.fillRect(QRect(1 + (j)*(m_block_w),
                                      1 + (i)*(m_block_h),
                                      m_block_w,
                                      m_block_h), drcolor);
//                } else
//                    cout << "0";
            }
//            cout << endl;
        }

        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                if (m_drawing_shape[i][j])
                    qp.fillRect(QRect(1 + (m_shape_x + j)*m_block_w,
                                      1 + (m_shape_y + i)*m_block_h,
                                      m_block_w,
                                      m_block_h), drcolor);

        // draw border
        // vertical
        qp.setPen(QPen(Qt::gray));
        for (int i = 1; i < SCENE_WIDTH; i++)
            qp.drawLine(1 + i*m_block_w, 0, 1 + i*m_block_w, height() - 1);
        // horizontal
        for (int i = 1; i < SCENE_HEIGHT; i++)
            qp.drawLine(1, i*m_block_h, width() - 1, i*m_block_h);
    }
}

// TNextFigureScene

// класс следующей фигуры
// устанавливает тип рисуемой фигуры
void TNextFigureScene::set_figure(int new_type) {
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            m_shape_map[i][j] = 0;
    m_figure_type = new_type;

    switch (m_figure_type) {

            case U_UNDEFINDED:
            case U_SPAWNERROR:
                exit(GAME_INTERNAL_ERR);
                break;

            case U_SQUARE: {
                m_shape_map[0][0] = 1; // 1100
                m_shape_map[0][1] = 1; // 1100
                m_shape_map[1][0] = 1; // 0000
                m_shape_map[1][1] = 1; // 0000
            }
                break;

            case U_SQUIGGLE:  {
                m_shape_map[0][0] = 1; // 1100
                m_shape_map[0][1] = 1; // 0110
                m_shape_map[1][1] = 1; // 0000
                m_shape_map[1][2] = 1; // 0000
            }
                break;

            case U_RSQUIGGLE: {
                m_shape_map[0][1] = 1; // 0110
                m_shape_map[0][2] = 1; // 1100
                m_shape_map[1][0] = 1; // 0000
                m_shape_map[1][1] = 1; // 0000
            }
                break;

            case U_LBLOCK: {
                m_shape_map[0][2] = 1; // 0010
                m_shape_map[1][0] = 1; // 1110
                m_shape_map[1][1] = 1; // 0000
                m_shape_map[1][2] = 1; // 0000
            }
                break;

            case U_RLBLOCK: {
                m_shape_map[0][0] = 1; // 1000
                m_shape_map[1][0] = 1; // 1110
                m_shape_map[1][1] = 1; // 0000
                m_shape_map[1][2] = 1; // 0000
            }
                break;

            case U_TBLOCK: {
                m_shape_map[0][1] = 1; // 0100
                m_shape_map[1][0] = 1; // 1110
                m_shape_map[1][1] = 1; // 0000
                m_shape_map[1][2] = 1; // 0000
            }
                break;

            case U_LINEPIECE: {
                m_shape_map[0][0] = 1; // 1111
                m_shape_map[0][1] = 1; // 0000
                m_shape_map[0][2] = 1; // 0000
                m_shape_map[0][3] = 1; // 0000
            }
                break;

    } // switch(figure_type)
}

// переопределенное событие отрисовки, для рисования следующей фигуры
void TNextFigureScene::paintEvent(QPaintEvent *) {
    if (m_started) {
        QPainter qp(this);
        qp.drawRect(QRect(0, 0, width() - 1, height() - 1));

        QColor drcolor;
        drcolor.setRgb(136, 136, 189);

        int _block_w = 12;
        int _block_h = 12;

        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                if (m_shape_map[i][j])
                    qp.fillRect(QRect(1 + (1 + j)*_block_w,
                                      1 + (1 + i)*_block_h,
                                      _block_w,
                                      _block_h), drcolor);

    } // if
}
